HTTP Strict Transport Security, or more commonly known as HSTS, is a security policy mechanism for websites whose sole purpose is to aid websites in protecting themselves from cookie hijacking and protocol downgrade attacks. Another purpose is that it allows the servers to announce that only web browsers with secure HTTPS connection should interact and they should never interact via insecure HTTP protocol. 

In order to communicate the HSTS policy to the user agent by the server, an HTTPS response header by the name of “Strict Transport Security” is used. These policies are sent to the users so that the user agent knows that they have a specified time during which they can access the servers in a secure manner. 

**Why Every Website Needs HSTS?**

Essentially, there are two ways through which a user can access a website. It can be through either HTTP or HTTPS. Now, the name clearly shows that both, in essence, are almost the same. The difference is the use of the encryption technology called TLS or SSL. 

Nowadays, you might observe that more and more websites seem to be of the opinion that using HSTS is safe. This is because they find that HSTS can help them keep safe by keeping browsing history private. It can also block ads that start playing as soon as you load the website or malware that get attached to a website which is then transported to the computer if these websites are visited. Moreover, HSTS can protect confidential data of the user. 

When one uses HTTPS, there is a slight flaw in the design of how HTTP and HTTPS work which can put users at serious risk. When you use a browser and type in a website with HTTPS, the browser first tries to open it with HTTP which is insecure. Only when the request is denied does the browser try again with the secure HTTPS protocol. This is because of the fact that a browser can’t tell if the website supports HTTP or HTTPs, so it tries HTTP first. 

This is a problem due to the fact that not only do you waste a lot of bytes and precious time, but you also put the website at risk against attacks. This is where HSTS comes in to save the day. HSTS reminds the browser time and time again that websites are capable of handling HTTPS. This means that whenever you type in HTTPS in your browser, instead of first trying to open the website with HTTP, it would directly open it with HTTPS. 

Be sure to enable HTTPS in all the subdomains before enabling HSTS. This is because using HSTS on a browser where HTTPS is not enabled would mean that the browser won’t use HTTP at all and thus, you would be stuck as you won't be able to access the site anymore. 

Example of the websites having HSTS [https://silverbullet-express.co.uk/](https://silverbullet-express.co.uk/)